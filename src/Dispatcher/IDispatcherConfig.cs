namespace AcklenAvenue.Dispatch
{
    public interface IDispatcherConfig
    {
        IDispatcherLogger Logger { get; }
        int MaximumHandlersPerDispatch { get; }
        int MinimumHandlersPerDispatch { get; }
    }
}