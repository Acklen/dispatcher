namespace AcklenAvenue.Dispatch
{
    public class DefaultDispatcherConfig : IDispatcherConfig
    {
        public DefaultDispatcherConfig()
        {
            Logger = new QuietDispatcherLogger();
            MaximumHandlersPerDispatch = 1;
            MinimumHandlersPerDispatch = 1;
        }
            
        public IDispatcherLogger Logger { get; }
        public int MaximumHandlersPerDispatch { get; }
        public int MinimumHandlersPerDispatch { get; }
                        
    }
}