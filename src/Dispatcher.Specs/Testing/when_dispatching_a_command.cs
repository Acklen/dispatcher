﻿using System.Collections.Generic;
using AcklenAvenue.Commands.Specs.Stubs;
using AcklenAvenue.Dispatch;
using Machine.Specifications;

namespace AcklenAvenue.Commands.Specs.Testing
{
    public class when_dispatching_a_command
    {
        static Dispatcher _dispatcher;
        static TestHandler _testHandler;
        static readonly TestCommand TestCommand = new TestCommand();

        Establish context =
            () =>
            {
                _testHandler = new TestHandler();
                var commandHandlers = new List<IHandler>
                {
                    _testHandler,
                    new AnotherTestHandler()
                };
                _dispatcher = new Dispatcher(new DefaultHandlerMatcher(commandHandlers));
            };

        Because of =
            () => _dispatcher.Dispatch(TestCommand).Await();

        It should_dispatch_the_expected_command =
            () => _testHandler.CommandHandled.ShouldEqual(TestCommand);
    }
}