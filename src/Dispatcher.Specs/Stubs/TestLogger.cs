﻿using System;
using AcklenAvenue.Dispatch;

namespace AcklenAvenue.Commands.Specs.Stubs
{
    public class TestLogger : IDispatcherLogger
    {
        public void LogInfo(object sender, DateTime timeStamp, string message, object command)
        {
            Console.WriteLine(message);
        }

        public void LogException(object sender, DateTime timeStamp, Exception exception, object command)
        {
            Console.WriteLine(exception.Message);
        }

    }
}