using System.Threading.Tasks;

namespace AcklenAvenue.Dispatch
{
    public interface ICommandValidator
    {
    }

    public interface ICommandValidator<in TCommand> : ICommandValidator
    {
        Task Validate(TCommand command);
    }
}